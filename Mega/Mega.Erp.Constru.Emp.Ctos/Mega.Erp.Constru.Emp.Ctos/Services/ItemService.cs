﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.Constru.Emp.Ctos.Repository;
using Mega.Erp.Constru.Emp.Ctos.Models;

namespace Mega.Erp.Constru.Emp.Ctos.Services
{
    public class ItemService
    {
        public IItemRepository _repository;

        public ItemService(IItemRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Item> ListaItens(Contrato cto)
        {
            return _repository.ListaItens(cto);
        }
    }
}
