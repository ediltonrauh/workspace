﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.Constru.Emp.Ctos.Repository;
using Mega.Erp.Constru.Emp.Ctos.Models;
using Oracle.ManagedDataAccess.Client;
using Dapper;

namespace Mega.Erp.Constru.Emp.Ctos.Repository
{
    public class ContratoRepository : IContratoRepository
    {
        public OracleConnection _connect;

        public ContratoRepository(OracleConnection connect)
        {
            _connect = connect;
        }
        public Contrato Get(decimal id)
        {
            return _connect.QueryFirst<Contrato>(@"Select  org_tab_in_codigo, 
                                                            org_pad_in_codigo ,
                                                            org_in_codigo ,
                                                            org_tau_st_codigo ,
                                                            cto_in_codigo ,
                                                            agn_tab_in_codigo ,
                                                            agn_pad_in_codigo ,
                                                            agn_tau_st_codigo ,
                                                            agn_in_codigo ,
                                                            pai_cto_in_codigo ,
                                                            adi_cto_in_codigo ,
                                                            cto_dt_inicio ,
                                                            cto_dt_final ,
                                                            cto_ch_status ,
                                                            cto_dt_status ,
                                                            cto_ch_origem ,
                                                            cto_ch_tipoemp ,
                                                            cto_ch_tipoobra ,
                                                            est_in_codigo ,
                                                            fil_in_codigo ,
                                                            tes_in_codigo ,
                                                            cus_tab_in_codigo ,
                                                            cus_pad_in_codigo ,
                                                            cus_ide_st_codigo ,
                                                            cus_in_reduzido ,
                                                            pro_tab_in_codigo ,
                                                            pro_pad_in_codigo ,
                                                            pro_ide_st_codigo ,
                                                            pro_in_reduzido  
                                                   from mgemp.emp_contrato
                                          where cto_in_codigo = :id", new { id = id });
        }
        public IEnumerable<Contrato> Get()
        {
            return _connect.Query<Contrato>(@"Select  org_tab_in_codigo, 
                                                            org_pad_in_codigo ,
                                                            org_in_codigo ,
                                                            org_tau_st_codigo ,
                                                            cto_in_codigo ,
                                                            agn_tab_in_codigo ,
                                                            agn_pad_in_codigo ,
                                                            agn_tau_st_codigo ,
                                                            agn_in_codigo ,
                                                            pai_cto_in_codigo ,
                                                            adi_cto_in_codigo ,
                                                            cto_dt_inicio ,
                                                            cto_dt_final ,
                                                            cto_ch_status ,
                                                            cto_dt_status ,
                                                            cto_ch_origem ,
                                                            cto_ch_tipoemp ,
                                                            cto_ch_tipoobra ,
                                                            est_in_codigo ,
                                                            fil_in_codigo ,
                                                            tes_in_codigo ,
                                                            cus_tab_in_codigo ,
                                                            cus_pad_in_codigo ,
                                                            cus_ide_st_codigo ,
                                                            cus_in_reduzido ,
                                                            pro_tab_in_codigo ,
                                                            pro_pad_in_codigo ,
                                                            pro_ide_st_codigo ,
                                                            pro_in_reduzido  
                                                   from mgemp.emp_contrato");
        }
        public IEnumerable<Contrato> Get(string filtro)
        {
            string sql = "Select  org_tab_in_codigo, org_pad_in_codigo , org_in_codigo , org_tau_st_codigo ,cto_in_codigo ,agn_tab_in_codigo ,agn_pad_in_codigo ,agn_tau_st_codigo , agn_in_codigo ,pai_cto_in_codigo ,adi_cto_in_codigo ,cto_dt_inicio , cto_dt_final , cto_ch_status ,cto_dt_status ,cto_ch_origem ,cto_ch_tipoemp , cto_ch_tipoobra , est_in_codigo ,fil_in_codigo ,tes_in_codigo , cus_tab_in_codigo , cus_pad_in_codigo ,cus_ide_st_codigo ,cus_in_reduzido ,pro_tab_in_codigo ,pro_pad_in_codigo ,pro_ide_st_codigo , pro_in_reduzido from mgemp.emp_contrato  ";
       
            return _connect.Query<Contrato>(sql + filtro);
        }
    }
}
