﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.Constru.Emp.Ctos.Models;

namespace Mega.Erp.Constru.Emp.Ctos.Repository
{
    public interface IApropriacaoItemRepository
    {
        IEnumerable<ApropriacaoItem> ListaApropriacao(Item item);
    }
}
