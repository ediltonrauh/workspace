﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Oracle.ManagedDataAccess.Client;
using Mega.Erp.Constru.Emp.Ctos.Models;

namespace Mega.Erp.Constru.Emp.Ctos.Repository
{
    public class ApropriacaoItemRepository : IApropriacaoItemRepository
    {
        public OracleConnection _connect;

        public ApropriacaoItemRepository(OracleConnection connect)
        {
            _connect = connect;
        }

        public IEnumerable<ApropriacaoItem> ListaApropriacao(Item item )
        {
            return _connect.Query<ApropriacaoItem>(@"select a.svc_in_codigo
                                                   , round(a.cia_re_quantidade, 20) cia_re_quantidade
                                                     , a.orc_in_codigo
                                                     , a.orc_in_sequencia
                                                from mgemp.emp_contrato_item_aprop a
                                                where a.cto_in_codigo      = :cto_in_codigo
                                                  and a.pro_in_codigo       = :pro_in_codigo
                                                  and a.ctoit_in_agrupador = :ctoit_in_agrupador", item);
             
        }
    }
}
