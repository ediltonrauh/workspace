﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.Constru.Emp.Ctos.Models;

namespace Mega.Erp.Constru.Emp.Ctos.Repository
{
    public interface IContratoRepository
    {
        IEnumerable<Contrato> Get();
        Contrato Get(decimal id);
        IEnumerable<Contrato> Get(string filtro);
        
    }
}
