﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.Constru.Emp.Ctos.Models
{
    public class ApropriacaoItem
    {
        
        public double cia_re_quantidade { get; set; }
        public decimal svc_in_codigo { get; set; }
        public decimal orc_in_codigo { get; set; }
        public int orc_in_sequencia{ get; set; }


    }
}
