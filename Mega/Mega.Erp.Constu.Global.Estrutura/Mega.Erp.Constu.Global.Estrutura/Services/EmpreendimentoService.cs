﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.Constu.Global.Estrutura.Repository;
using Mega.Erp.Constu.Global.Estrutura.Models;

namespace Mega.Erp.Constu.Global.Estrutura.Services
{
    public class EmpreendimentoService
    {
        public IEmpreendimentoRepository _repository;

        public EmpreendimentoService (IEmpreendimentoRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Bloco> ListaBlocoEmp (List<decimal> emp)
        {
            return _repository.BuscaBlocos(emp);
        }

        public Empreendimento BuscaEmpreendimento(decimal id)
        {
            return _repository.BuscaEmpreendimento(id);
        }

    }
}
