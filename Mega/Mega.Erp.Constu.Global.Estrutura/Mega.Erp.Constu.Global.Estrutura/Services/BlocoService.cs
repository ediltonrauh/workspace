﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.Constu.Global.Estrutura.Repository;
using Mega.Erp.Constu.Global.Estrutura.Models;

namespace Mega.Erp.Constu.Global.Estrutura.Services
{
    public class BlocoService
    {
        public IBlocoRepository _repository;

        public BlocoService(IBlocoRepository repository)
        {
            _repository = repository;
        }

        public List<Bloco> ListaBlocos(Empreendimento emp)
        {
            return _repository.ListaBlocoEmp(emp);
        }

        public IEnumerable<Bloco> ListaBloco(decimal emp)
        {
            return _repository.ListaBloco(emp);
        }
    }
}
