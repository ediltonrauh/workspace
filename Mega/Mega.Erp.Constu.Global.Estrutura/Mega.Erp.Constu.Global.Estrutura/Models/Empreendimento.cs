﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.Constu.Global.Estrutura.Models
{
    public class Empreendimento
    {
        public decimal id { get; set; }
        public string nome { get; set; }
        public string codigo { get; set; }
        public List<Bloco> listaBloco {get; set;}

    }
}
