﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.Constu.Global.Estrutura.Models;

namespace Mega.Erp.Constu.Global.Estrutura.Repository
{
    public interface IBlocoRepository
    {
        List<Bloco> ListaBlocoEmp(Empreendimento emp);
        IEnumerable<Bloco> ListaBloco(decimal estCodigo);
    }
}
