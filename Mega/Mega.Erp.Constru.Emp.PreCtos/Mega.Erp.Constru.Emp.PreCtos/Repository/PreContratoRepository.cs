﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using Dapper;
using Mega.Erp.Constru.Emp.PreCtos.Models;
using Mega.Erp.Constu.Global.Estrutura.Models;
using Mega.Erp.Constru.Emp.Ctos.Models;

namespace Mega.Erp.Constru.Emp.PreCtos.Repository
{
    public class PreContratoRepository : IPreContratoRepository
    {
        public OracleConnection _connect;

        public PreContratoRepository(OracleConnection connect)
        {
            _connect = connect;
        }

        //public IEnumerable<PreContrato> ListaBlocos( IEnumerable<PreContrato> preContratos)
        //{

        //    for(int i = 0; i < preContratos.Count(); i++)
        //    {
                
        //        preContratos.ElementAt(i).blocos = _connect.Query<Bloco>(@"Select  BLO_NOME      nome,
        //                                                                           BLO_ST_CODIGO codigo,
        //                                                                           EMP.EST_IN_CODIGO est_in_codigo                                                   
      
        //                                                                      FROM MGDBM.DBM_ESTRUTURA EMP,
        //                                                                           (SELECT TAB_IN_CODIGO,
        //                                                                                   PAD_IN_CODIGO,
        //                                                                                   IN_CODIGO,
        //                                                                                   TAU_ST_CODIGO,
        //                                                                                   ETP.EST_IN_CODIGO ETP_CODIGO,
        //                                                                                   ETP.EST_ST_CODIGO ETP_ST_CODIGO,
        //                                                                                   ETP.EST_ST_NOME ETP_NOME,
        //                                                                                   BLO_CODIGO,
        //                                                                                   BLO_ST_CODIGO,
        //                                                                                   BLO_NOME,
        //                                                                                   ETP.PAI_EST_IN_CODIGO ETP_PAI_EST
        //                                                                              FROM MGDBM.DBM_ESTRUTURA ETP,
        //                                                                                   (SELECT BLO.ORG_TAB_IN_CODIGO TAB_IN_CODIGO,
        //                                                                                           BLO.ORG_PAD_IN_CODIGO PAD_IN_CODIGO,
        //                                                                                           BLO.ORG_IN_CODIGO     IN_CODIGO,
        //                                                                                           BLO.ORG_TAU_ST_CODIGO TAU_ST_CODIGO,
        //                                                                                           BLO.EST_IN_CODIGO     BLO_CODIGO,
        //                                                                                           BLO.EST_ST_CODIGO     BLO_ST_CODIGO,
        //                                                                                           BLO.EST_ST_NOME       BLO_NOME,
        //                                                                                           BLO.PAI_EST_IN_CODIGO BLO_PAI_EST
        //                                                                                      FROM MGDBM.DBM_ESTRUTURA  BLO
        //                                                                                     WHERE BLO.EST_CH_TIPOESTRUTURA = 'B')
               
        //                                                                             WHERE ETP.ORG_TAB_IN_CODIGO = TAB_IN_CODIGO
        //                                                                               AND ETP.ORG_PAD_IN_CODIGO = PAD_IN_CODIGO
        //                                                                               AND ETP.ORG_IN_CODIGO     = IN_CODIGO
        //                                                                               AND ETP.ORG_TAU_ST_CODIGO = TAU_ST_CODIGO
        //                                                                               AND ETP.EST_IN_CODIGO     = BLO_PAI_EST
        //                                                                               AND ETP.EST_CH_TIPOESTRUTURA = 'T')
        //                                                                     WHERE EMP.ORG_TAB_IN_CODIGO = TAB_IN_CODIGO
        //                                                                       AND EMP.ORG_PAD_IN_CODIGO = PAD_IN_CODIGO
        //                                                                       AND EMP.ORG_IN_CODIGO = IN_CODIGO
        //                                                                       AND EMP.ORG_TAU_ST_CODIGO = TAU_ST_CODIGO
        //                                                                       AND EMP.EST_IN_CODIGO = ETP_PAI_EST
        //                                                                       AND EMP.EST_CH_TIPOESTRUTURA = 'E'
        //                                                                       AND EMP.EST_IN_CODIGO = :vari ", new { vari = preContratos.ElementAt(i).est_in_codigo });
        //    }




        //    return preContratos;
        //}
        //public IEnumerable<PreContrato> ListaItens(IEnumerable<PreContrato> preContratos)
        //{
        //    for (int i = 0; i < preContratos.Count(); i++)
        //    {

        //        preContratos.ElementAt(i).itens = _connect.Query<Item>(@"select i.cto_in_codigo
        //                                                                         , i.ctoit_in_codigo
        //                                                                         , i.pro_in_codigo
        //                                                                         , i.uni_st_unidade
        //                                                                         , i.ctoit_st_descricao
        //                                                                    from mgemp.emp_contrato_item i
        //                                                                  where i.cto_in_codigo = :vari ", new { vari = preContratos.ElementAt(i).cto_in_codigo });
        //    }




        //    return preContratos;
        //}

    }
}
