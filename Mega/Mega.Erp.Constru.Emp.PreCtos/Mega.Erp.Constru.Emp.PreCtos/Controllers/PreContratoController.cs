﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mega.Erp.Constru.Emp.Ctos.Models;
using Mega.Erp.Constru.Emp.PreCtos.Models;
using Mega.Erp.Constru.Emp.PreCtos.Services;
using Mega.Erp.Constru.Emp.PreCtos.Repository;
using Mega.Erp.Constu.Global.Estrutura.Models;

namespace Mega.Erp.Constru.Emp.PreCtos.Controllers
{
    [Route("api/[controller]")]
    public class PreContratoController : Controller
    {
        public PreContratoService _service;
        public PreContratoController(PreContratoService service)
        {
            _service = service;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var result = new Constu.Global.Estrutura.Models.ServiceResult();
            try
            {

                result.Result = _service.GetPreContratos(((PreContratoRepository)this._service._repository));
                return Ok(result);
            }
            catch (Exception ex)
            {
                result.Error = ex;
                return NotFound(result);
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] IEnumerable<PreContrato> precto)
        {
            var result = new Constu.Global.Estrutura.Models.ServiceResult();
            try
            {

                result.Result = _service.GetBlocos(((PreContratoRepository)this._service._repository), precto);
                return Ok(result);
            }
            catch (Exception ex)
            {
                result.Error = ex;
                return NotFound(result);
            }
        }

        [HttpPut]
        public IActionResult Put([FromBody] IEnumerable<PreContrato> precto)
        {
            var result = new Constu.Global.Estrutura.Models.ServiceResult();
            try
            {

                _service.SalvaContratos(((PreContratoRepository)this._service._repository), precto);
                result.Result = "Salvo com sucesso";
                return Ok(result);
            }
            catch (Exception ex)
            {
                result.Error = ex;
                return NotFound(result);
            }
        }

    }
}
