using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mega.Erp.Global.Api.Services;
using Mega.Erp.Global.Api.Models;
using AutoMapper;

namespace Mega.Erp.Global.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class DepositoController : Controller
    {
        private readonly IDepositoService _depositoServico;
        private readonly IMapper _mapper;

        public DepositoController(IDepositoService depositoServico, IMapper mapper)
        {
            _depositoServico = depositoServico;
            _mapper = mapper;
        }

        // GET: api/Deposito
        [HttpGet]
        public IEnumerable<DepositoDTO> Get()
        {
            IEnumerable<Deposito> result = _depositoServico.ObterTodosDepositos();
            return _mapper.Map<List<DepositoDTO>>(result);
        }

        // GET: api/Deposito/5
        [HttpGet("{id}", Name = "GetDeposito")]
        public IActionResult Get(int id)
        {
            var result = _depositoServico.ObterDepositoPeloId(id);
            if (result == null)
                return new NotFoundResult();
            else
                return new ObjectResult(_mapper.Map<DepositoDTO>(result));
        }
        
        // POST: api/Deposito
        [HttpPost]
        public IActionResult Post([FromBody]DepositoDTO value)
        {
            var result = _depositoServico.InserirDeposito(_mapper.Map<Deposito>(value));
            if (result != null)
                return CreatedAtRoute("GetDeposito", new { id = result.Id }, _mapper.Map<DepositoDTO>(result));
            else
                return BadRequest();
        }
        
        // PUT: api/Deposito/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]DepositoDTO value)
        {
            var result = _depositoServico.AtualizarDeposito(id, _mapper.Map<Deposito>(value));
            if (result)
                return new NoContentResult();
            else
                return BadRequest();
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var result = _depositoServico.ExcluirDeposito(id);
            if (result)
                return new OkResult();
            else
                return BadRequest();
        }
    }
}