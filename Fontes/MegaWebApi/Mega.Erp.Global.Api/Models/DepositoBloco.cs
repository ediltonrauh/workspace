﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Erp.Global.Api.Models
{
    public partial class DepositoBloco : ChaveEstrutura
    {
        // Foreign key
        public int? DepositoId { get; set; }
        private Deposito Deposito { get; set; }
    }
}