﻿using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;

namespace Mega.Erp.Global.Api.Models
{
    public partial class Deposito
    {
        public Deposito()
        {
            DepositoBloco = new HashSet<DepositoBloco>();
            DepositoFilial = new HashSet<DepositoFilial>();
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public DateTime? DataCadastro { get; set; }
        public decimal? Area { get; set; }
        public string Status { get; set; }
        public string TipoAcesso { get; set; }

        public virtual ICollection<DepositoBloco> DepositoBloco { get; set; }
        public virtual ICollection<DepositoFilial> DepositoFilial { get; set; }
    }

    public class DepositoDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }

        public virtual ICollection<DepositoBloco> DepositoBloco { get; set; }
        public virtual ICollection<DepositoFilial> DepositoFilial { get; set; }
    }
}