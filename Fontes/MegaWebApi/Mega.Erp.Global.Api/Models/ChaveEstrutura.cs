﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.Global.Api.Models
{
    public partial class ChaveEstrutura
    {
        public int CodigoTab { get; set; }
        public int CodigoPadrao { get; set; }
        public int CodigoOrganizacao { get; set; }
        public string CodigoTau { get; set; }
        public int EstruturaId { get; set; }
    }
}