﻿using Mega.Erp.Global.Api.Models;
using Mega.Erp.Global.Api.Models.Interfaces;
using System;

namespace Mega.Erp.Global.Api.Infrastructure.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<Deposito> DepositoRepository { get; }
        IGenericRepository<DepositoBloco> DepositoBlocoRepository { get; }
        IGenericRepository<DepositoFilial> DepositoFilialRepository { get; }
        int Commit();
    }
}