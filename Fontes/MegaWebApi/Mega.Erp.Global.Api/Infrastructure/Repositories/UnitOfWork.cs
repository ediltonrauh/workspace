﻿using Mega.Erp.Global.Api.Infrastructure;
using Mega.Erp.Global.Api.Models;
using Mega.Erp.Global.Api.Models.Interfaces;
using System;
using System.Diagnostics;

namespace Mega.Erp.Global.Api.Infrastructure.Repositories
{
    public sealed class UnitOfWork : IDisposable, IUnitOfWork
    {
        private MgContext _context;
        private IGenericRepository<Deposito> _depositoRepository;
        private IGenericRepository<DepositoBloco> _depositoBlocoRepository;
        private IGenericRepository<DepositoFilial> _depositoFilialRepository;

        public UnitOfWork(MgContext context)
        {
            _context = context;
        }

        public IGenericRepository<Deposito> DepositoRepository
        {
            get
            {
                if (_depositoRepository == null)
                    _depositoRepository = new GenericRepository<Deposito>(_context);
                return _depositoRepository;
            }
        }

        public IGenericRepository<DepositoBloco> DepositoBlocoRepository
        {
            get
            {
                if (_depositoBlocoRepository == null)
                    _depositoBlocoRepository = new GenericRepository<DepositoBloco>(_context);
                return _depositoBlocoRepository;
            }
        }

        public IGenericRepository<DepositoFilial> DepositoFilialRepository
        {
            get
            {
                if (_depositoFilialRepository == null)
                    _depositoFilialRepository = new GenericRepository<DepositoFilial>(_context);
                return _depositoFilialRepository;
            }
        }

        public int Commit()
        {
            return _context.SaveChanges();
        }

        #region Dispose
        private bool disposed = false;

        private void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("UnitOfWork está sendo descartado");
                    _context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}