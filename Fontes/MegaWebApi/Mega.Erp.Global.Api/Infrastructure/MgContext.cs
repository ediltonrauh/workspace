﻿using Mega.Erp.Global.Api.Mapping;
using Mega.Erp.Global.Api.Models;
using Microsoft.EntityFrameworkCore;

namespace Mega.Erp.Global.Api.Infrastructure
{
    public class MgContext : DbContext
    {
        public MgContext(DbContextOptions<MgContext> options)
           : base(options)
        {
        }

        public DbSet<Deposito> Depositos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.AddConfiguration(new DepositoMap());
            modelBuilder.AddConfiguration(new DepositoBlocoMap());
            modelBuilder.AddConfiguration(new DepositoFilialMap());
        }
    }
}