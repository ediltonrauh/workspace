﻿using Mega.Erp.Global.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Mega.Erp.Global.Api.Mapping
{
    public class DepositoMap : EntityTypeConfiguration<Deposito>
    {
        public override void Map(EntityTypeBuilder<Deposito> builder)
        {
            builder.ToTable("DBM_DEPOSITO", "MGDBM");

            // Primary Key
            builder.HasKey(b => new { b.Id });
            builder.Property(b => b.Id)
               .HasColumnName("DEP_IN_CODIGO");

            builder.Property(b => b.Id)
                .HasColumnName("DEP_IN_CODIGO");

            builder.Property(b => b.Nome)
                .HasColumnName("DEP_ST_NOME");

            builder.Property(b => b.DataCadastro)
                .HasColumnName("DEP_DT_CADASTRO");

            builder.Property(b => b.Area)
                .HasColumnName("DEP_RE_AREA");

            builder.Property(b => b.Status)
                .HasColumnName("DEP_CH_STATUS")
                .HasDefaultValue("D");

            builder.Property(b => b.TipoAcesso)
                .HasColumnName("DEP_CH_TIPOACESSO")
                .HasDefaultValue("G");
        }
    }
}