﻿using Mega.Erp.Global.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Mega.Erp.Global.Api.Mapping
{
    public class DepositoFilialMap : EntityTypeConfiguration<DepositoFilial>
    {
        public override void Map(EntityTypeBuilder<DepositoFilial> builder)
        {
            builder.ToTable("DBM_DEPOSITO_FILIAL", "MGDBM");

            // Primary Key
            builder.HasKey(b => new { b.Id, b.DepositoId });

            builder.Property(b => b.Id)
                .HasColumnName("GRU_IN_CODIGO")
                .IsRequired();

            builder.Property(b => b.DepositoId)
                .HasColumnName("DEP_IN_CODIGO")
                .IsRequired();
        }
    }
}