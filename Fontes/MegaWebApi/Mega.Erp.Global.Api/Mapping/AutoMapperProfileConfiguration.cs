﻿using AutoMapper;
using Mega.Erp.Global.Api.Models;

namespace Mega.Erp.Global.Api.Mapping
{
    public class AutoMapperProfileConfiguration : Profile
    {
        public AutoMapperProfileConfiguration()
        : this("MyProfile")
        {
        }
        protected AutoMapperProfileConfiguration(string profileName)
        : base(profileName)
        {
            CreateMap<DepositoDTO, Deposito>();
            CreateMap<Deposito, DepositoDTO>();
        }
    }
}