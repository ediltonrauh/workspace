﻿using Mega.Erp.Global.Api.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Mega.Erp.Global.Api.Mapping
{
    public class DepositoBlocoMap : EntityTypeConfiguration<DepositoBloco>
    {
        public override void Map(EntityTypeBuilder<DepositoBloco> builder)
        {
            builder.ToTable("DBM_DEPOSITO_BLOCO", "MGDBM");

            // Primary Key
            builder.HasKey(b => new { b.CodigoTab, b.CodigoPadrao, b.CodigoOrganizacao, b.CodigoTau, b.EstruturaId, b.DepositoId });

            builder.Property(b => b.CodigoTab)
                .HasColumnName("ORG_TAB_IN_CODIGO");

            builder.Property(b => b.CodigoPadrao)
                .HasColumnName("ORG_PAD_IN_CODIGO");

            builder.Property(b => b.CodigoOrganizacao)
                .HasColumnName("ORG_IN_CODIGO");

            builder.Property(b => b.CodigoTau)
                .HasColumnName("ORG_TAU_ST_CODIGO");

            builder.Property(b => b.EstruturaId)
                .HasColumnName("EST_IN_CODIGO")
                .IsRequired();

            builder.Property(b => b.DepositoId)
                .HasColumnName("DEP_IN_CODIGO")
                .IsRequired();
        }
    }
}