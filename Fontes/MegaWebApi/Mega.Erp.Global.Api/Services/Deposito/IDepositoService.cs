﻿using System.Collections.Generic;
using Mega.Erp.Global.Api.Models;

namespace Mega.Erp.Global.Api.Services
{
    public interface IDepositoService
    {
        Deposito ObterDepositoPeloId(int id);
        IEnumerable<Deposito> ObterTodosDepositos();
        bool ExcluirDeposito(int id);
        Deposito InserirDeposito(Deposito value);
        bool AtualizarDeposito(int id, Deposito value);
    }
}