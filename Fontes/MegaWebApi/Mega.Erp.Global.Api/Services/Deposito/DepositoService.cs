﻿using System.Collections.Generic;
using System.Linq;
using System;
using Mega.Erp.Global.Api.Infrastructure.Repositories;
using Mega.Erp.Global.Api.Models;

namespace Mega.Erp.Global.Api.Services
{
    public class DepositoService : IDepositoService
    {
        private readonly IUnitOfWork _unitOfWork;

        public DepositoService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Deposito ObterDepositoPeloId(int id)
        {
            var item = _unitOfWork.DepositoRepository.GetSingle(q=> q.Id > 0, null, i=> i.DepositoBloco, i=> i.DepositoFilial);
            if (item != null)
            {
                return item;
            }
            return null;
        }

        public IEnumerable<Deposito> ObterTodosDepositos()
        {
            var items = _unitOfWork.DepositoRepository.Get();
            if (items.Any())
            {
                return items;
            }
            return null;
        }

        public bool ExcluirDeposito(int id)
        {
            var success = false;
            if (id > 0)
            {
                var item = _unitOfWork.DepositoRepository.Get(id);
                if (item != null)
                {
                    _unitOfWork.DepositoRepository.Delete(item);
                    _unitOfWork.Commit();
                    success = true;
                }
            }
            return success;
        }

        public Deposito InserirDeposito(Deposito value)
        {
            value.DataCadastro = DateTime.Now;

            _unitOfWork.DepositoRepository.Insert(value);
            if (value.TipoAcesso == "B" && value.DepositoBloco != null)
            {
                foreach (DepositoBloco depBloco in value.DepositoBloco)
                {
                    _unitOfWork.DepositoBlocoRepository.Insert(depBloco);
                }
            }
            else if (value.TipoAcesso == "F" && value.DepositoFilial != null)
            {
                foreach (DepositoFilial depFilial in value.DepositoFilial)
                {
                    _unitOfWork.DepositoFilialRepository.Insert(depFilial);
                }
            }
            _unitOfWork.Commit();
            return value;
        }

        public bool AtualizarDeposito(int id, Deposito value)
        {
            var success = false;
            if (value != null)
            {
                var item = _unitOfWork.DepositoRepository.Get(id);
                if (item != null)
                {
                    item.Nome = value.Nome;
                    item.DataCadastro = DateTime.Now;
                    item.TipoAcesso = value.TipoAcesso;
                    item.Area = value.Area;

                    _unitOfWork.DepositoBlocoRepository.Delete(p=> p.DepositoId == item.Id);
                    _unitOfWork.DepositoFilialRepository.Delete(p => p.DepositoId == item.Id);

                    if (value.TipoAcesso == "B" && value.DepositoBloco != null)
                    {
                        foreach (DepositoBloco depBloco in value.DepositoBloco)
                        {
                            _unitOfWork.DepositoBlocoRepository.Insert(depBloco);
                        }
                    }
                    else if (value.TipoAcesso == "F" && value.DepositoFilial != null)
                    {
                        foreach (DepositoFilial depFilial in value.DepositoFilial)
                        {
                            _unitOfWork.DepositoFilialRepository.Insert(depFilial);
                        }
                    }

                    _unitOfWork.DepositoRepository.Update(item);
                    _unitOfWork.Commit();
                    success = true;
                }
            }
            return success;
        }
    }
}