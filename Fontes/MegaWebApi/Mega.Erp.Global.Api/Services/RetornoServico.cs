﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.Global.Api.Services
{
    public class RetornoServico
    {
        public object Result { get; set; }
        public Exception Erro { get; set; }
    }
}