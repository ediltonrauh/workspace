﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.Constu.Global.Estrutura.Models;
namespace Mega.Erp.Constru.Emp.Ctos.Models
{
    public class Contrato
    {
        public int org_tab_in_codigo { get; set; }
        public int org_pad_in_codigo { get; set; }
        public decimal org_in_codigo { get; set; }
        public string org_tau_st_codigo { get; set; }
        public decimal cto_in_codigo { get; set; }
        public int? agn_tab_in_codigo { get; set; }
        public int? agn_pad_in_codigo { get; set; }
        public string agn_tau_st_codigo { get; set; }
        public decimal? agn_in_codigo { get; set; }
        public decimal? pai_cto_in_codigo { get; set; }
        public decimal? adi_cto_in_codigo { get; set; }
        public DateTime cto_dt_inicio { get; set; }
        public DateTime cto_dt_final { get; set; }
        public char cto_ch_status { get; set; }
        public DateTime cto_dt_status { get; set; }
        //public string cto_st_path_texto varchar2(255)
        public char cto_ch_origem { get; set; }
        public char cto_ch_tipoemp { get; set; }
        //public char cto_bo_outrosagentes char (1)
        //public char cto_bo_reajuste char (1)
        //public int cto_in_periodreaj number(3)
        public char cto_ch_tipoobra { get; set; }
        //public int ind_in_codigo number(3)
        public decimal? est_in_codigo { get; set; }
        public decimal? fil_in_codigo { get; set; }
        public decimal? tes_in_codigo { get; set; }
        //public int tpre_tab_in_codigo number(3)
        //public int tpre_pad_in_codigo number(3)
        //public decimal tpre_in_codigo number(22)
        public int cus_tab_in_codigo { get; set; }
        public int cus_pad_in_codigo { get; set; }
        public string cus_ide_st_codigo { get; set; }
        public decimal cus_in_reduzido { get; set; }
        public int pro_tab_in_codigo { get; set; }
        public int pro_pad_in_codigo { get; set; }
        public int pro_ide_st_codigo { get; set; }
        public decimal pro_in_reduzido { get; set; }

        public IEnumerable<Bloco> blocos { get; set; }
        public IEnumerable<Item> itens { get; set; }
        /*public decimal agnir_in_codigo number(7)        
        public agniss_in_codigo number(7)
        public agniss_tau_st_codigo varchar2(3)
        public public public agnir_tau_st_codigo varchar2(3)
        public sap_st_codigo varchar2(10)
        public cto_ch_periodomed char (1)
        public cto_st_alternativo varchar2(20)
        public cto_dt_basefator date
        public cto_bo_controlafator char (1)
        public cto_bo_controlapercmat char (1)
        public cto_bo_percmatdiferente char (1)
        public cto_bo_percmatacima char (1)
        public cto_bo_alterarpercmat char (1)
        public cto_ch_controlanfterc char (1)
        public cto_ch_acaolimnfterc char (1)
        public cond_tab_in_codigo number(3)
        public cond_pad_in_codigo number(3)
        public cond_st_codigo varchar2(10)
        public cto_re_tolerancianf number(8,4)
        public cto_bo_fechamedicao char (1)
        public cto_bo_sugereagente char (1)
        public cto_bo_faturamentodireto char (1)
        public cto_re_percnfterc number(22,8)
        public cto_re_vlrnfterc number(22,8)
        public cto_ch_recolheiss char (1)
        public acaon_tab_in_codigo number(3)
        public acaon_pad_in_codigo number(3)
        public acaon_in_codigo number(3)
        public cto_bo_outrosagentesnf char (1)
        public grq_tab_in_codigo number(3)
        public grq_pad_in_codigo number(3)
        public grq_in_codigo number(7)
        public cto_bo_agentes_fat_lista char (1)
        public cto_bo_usaagnfat_filho char (1)
        public cto_bo_contsaldoapr char (1)
        public cto_bo_bloqmovsemapr char (1)
        public cto_bo_bloqmovaprparcial char (1)
        public cto_bo_bloqratapr char (1)
        public cto_ch_fluxostatus char (1)
        public tdoc_in_codigo number
        public cto_bo_participafluxo char (1)
        public cto_in_padraomed number(3)
        public ecf_dt_base date
        public cto_st_objeto clob
        public cto_st_observacao clob
        public cto_ch_finaliza_valor char (1)
        public par_bo_contabiliza_medicao char (1)
        public acaoci_tab_in_codigo number(3)
        public acaoci_pad_in_codigo number(3)
        public acaoci_in_codigo number(3)
        public acaoce_tab_in_codigo number(3)
        public acaoce_pad_in_codigo number(3)
        public acaoce_in_codigo number(3)
        public data_criacao timestamp(6) with time zone
        public data_alteracao timestamp(6) with time zone
        public id_usuario_criacao number(5)
        public id_usuario_alteracao number(5)
        public data_envio_movel timestamp(6) with time zone
        public id_integracao varchar2(64)
        public enviar_movel char (1)
        public cto_bo_alocacao_equipamento char (1)
        public cto_ch_agrupa_adt_svc char (1)
        public cto_in_desc_fatdir number(22)
        public cto_bo_desc_somacus_fatdir char (1)*/

    }
}
