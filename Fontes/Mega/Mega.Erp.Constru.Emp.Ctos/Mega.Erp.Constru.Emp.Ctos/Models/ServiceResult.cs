﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.Constru.Emp.Ctos.Models
{
    public class ServiceResult
    {
        public object Result { get; set; }
        public Exception Error { get; set; }
    }
}
