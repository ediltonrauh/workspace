﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.Constru.Emp.Ctos.Models
{
    public class Item
    {
      public decimal cto_in_codigo { get; set; }
      public decimal ctoit_in_codigo { get; set; }
      public decimal pro_in_codigo { get; set; }
      public string uni_st_unidade { get; set; }
      public string ctoit_st_descricao { get; set; }
      public decimal ctoit_in_agrupador { get; set; }
      public IEnumerable<ApropriacaoItem> apropriacao { get; set; }
    }
}
