﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mega.Erp.Constru.Emp.Ctos.Services;
using Mega.Erp.Constru.Emp.Ctos.Models;
namespace Mega.Erp.Constru.Emp.Ctos.Controllers
{
    [Route("api/[controller]")]
    public class ContratoController : Controller
    {
        private ContratoService _service;

        public ContratoController (ContratoService service)
        {
            _service = service;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var result = new ServiceResult();
            try
            {
                result.Result = _service.Get();
                return Ok(result);
            }
            catch (Exception ex)
            {
                result.Error = ex;
                return NotFound(result);
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get(decimal id)
        {
            var result = new ServiceResult();
            try
            {
                result.Result = _service.Get(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                result.Error = ex;
                return NotFound(result);
            }
        }

         [HttpGet("{status}/{tipo}", Name = "statusCto")]
        //[HttpGet("{status}")]
        public IActionResult Get( string tipo)
        {
            var result = new ServiceResult();
            try
            {
                result.Result = _service.Get(tipo);
                return Ok(result);
            }
            catch (Exception ex)
            {
                result.Error = ex;
                return NotFound(result);
            }
        }
    }
}
