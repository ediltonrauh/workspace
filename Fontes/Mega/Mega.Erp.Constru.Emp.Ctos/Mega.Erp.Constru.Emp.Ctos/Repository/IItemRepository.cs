﻿using Mega.Erp.Constru.Emp.Ctos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.Constru.Emp.Ctos.Repository
{
    public interface IItemRepository
    {
        IEnumerable<Item> ListaItens(Contrato contrato);
    }
}
