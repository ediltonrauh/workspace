﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.Constru.Emp.Ctos.Models;
using Dapper;
using Oracle.ManagedDataAccess.Client;

namespace Mega.Erp.Constru.Emp.Ctos.Repository
{
    public class ItemRepository : IItemRepository
    {
        public OracleConnection _connect;

        public ItemRepository(OracleConnection connect)
        {
            _connect = connect;
        }

        public IEnumerable<Item> ListaItens(Contrato contrato)
        {

            return _connect.Query<Item>(@"select i.cto_in_codigo
                                                , i.ctoit_in_codigo
                                                , i.pro_in_codigo
                                                , i.uni_st_unidade
                                                , i.ctoit_st_descricao
                                                , i.ctoit_in_agrupador
                                        from mgemp.emp_contrato_item i
                                        where i.cto_in_codigo = :vari ", new { vari = contrato.cto_in_codigo });
            
        }
    }
}
