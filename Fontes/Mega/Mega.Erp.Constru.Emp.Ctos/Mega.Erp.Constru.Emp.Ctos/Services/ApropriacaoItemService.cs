﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.Constru.Emp.Ctos.Repository;
using Mega.Erp.Constru.Emp.Ctos.Models;

namespace Mega.Erp.Constru.Emp.Ctos.Services
{
    public class ApropriacaoItemService
    {
        public IApropriacaoItemRepository _repository;

        public ApropriacaoItemService(IApropriacaoItemRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<ApropriacaoItem> ListaApropriacao(Item item)
        {
            return _repository.ListaApropriacao(item);
        }
    }
}
