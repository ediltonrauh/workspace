﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.Constru.Emp.Ctos.Models;
using Mega.Erp.Constru.Emp.Ctos.Repository;

namespace Mega.Erp.Constru.Emp.Ctos.Services
{
    public class ContratoService
    {
        public IContratoRepository _repository;

        public ContratoService(IContratoRepository repository)
        {
            _repository = repository;
        }
        public IEnumerable<Contrato> Get()
        {
            IEnumerable<Contrato> objContrato = _repository.Get();
            return objContrato;
        }

        public Contrato Get(decimal id)
        {
            Contrato objContrato = _repository.Get(id);
            return objContrato;
        }
        public IEnumerable<Contrato> Get(string status)
        {
            string filtro = "";          

            if (status == "T")
            {
                IEnumerable<Contrato> objContrato = _repository.Get();
                return objContrato;
            }
            else
            {
                filtro = "where cto_ch_status =  " + "'"+status+"'";
                IEnumerable<Contrato> objContrato = _repository.Get(filtro);
                return objContrato;
            }
            
        }

        
    }
}
