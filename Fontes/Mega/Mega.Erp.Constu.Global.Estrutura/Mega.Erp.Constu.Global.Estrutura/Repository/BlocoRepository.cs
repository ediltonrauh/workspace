﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Oracle.ManagedDataAccess.Client;
using Mega.Erp.Constu.Global.Estrutura.Models;

namespace Mega.Erp.Constu.Global.Estrutura.Repository
{
    public class BlocoRepository : IBlocoRepository
    {
        public OracleConnection _connect;

        public BlocoRepository(OracleConnection connect)
        {
            _connect = connect;
        }

        public List<Bloco> ListaBlocoEmp(Empreendimento emp)
        {
            return _connect.Query<Bloco>(@"Select b.est_in_codigo
                                                , b.EST_ST_CODIGO
                                                , b.EST_ST_NOME   
                                        from mgdbm.dbm_estrutura b
                                        where b.PAI_EST_IN_CODIGO = :idEmpreendimento", new { idEmpreendimento = emp.id }).ToList();
            
        }

        public IEnumerable<Bloco> ListaBloco(decimal estCodigo)
        {

            return _connect.Query<Bloco>(@"Select  BLO_NOME      nome,
                                                                                   BLO_ST_CODIGO codigo,
                                                                                   EMP.EST_IN_CODIGO est_in_codigo                                                   
      
                                                                              FROM MGDBM.DBM_ESTRUTURA EMP,
                                                                                   (SELECT TAB_IN_CODIGO,
                                                                                           PAD_IN_CODIGO,
                                                                                           IN_CODIGO,
                                                                                           TAU_ST_CODIGO,
                                                                                           ETP.EST_IN_CODIGO ETP_CODIGO,
                                                                                           ETP.EST_ST_CODIGO ETP_ST_CODIGO,
                                                                                           ETP.EST_ST_NOME ETP_NOME,
                                                                                           BLO_CODIGO,
                                                                                           BLO_ST_CODIGO,
                                                                                           BLO_NOME,
                                                                                           ETP.PAI_EST_IN_CODIGO ETP_PAI_EST
                                                                                      FROM MGDBM.DBM_ESTRUTURA ETP,
                                                                                           (SELECT BLO.ORG_TAB_IN_CODIGO TAB_IN_CODIGO,
                                                                                                   BLO.ORG_PAD_IN_CODIGO PAD_IN_CODIGO,
                                                                                                   BLO.ORG_IN_CODIGO     IN_CODIGO,
                                                                                                   BLO.ORG_TAU_ST_CODIGO TAU_ST_CODIGO,
                                                                                                   BLO.EST_IN_CODIGO     BLO_CODIGO,
                                                                                                   BLO.EST_ST_CODIGO     BLO_ST_CODIGO,
                                                                                                   BLO.EST_ST_NOME       BLO_NOME,
                                                                                                   BLO.PAI_EST_IN_CODIGO BLO_PAI_EST
                                                                                              FROM MGDBM.DBM_ESTRUTURA  BLO
                                                                                             WHERE BLO.EST_CH_TIPOESTRUTURA = 'B')
               
                                                                                     WHERE ETP.ORG_TAB_IN_CODIGO = TAB_IN_CODIGO
                                                                                       AND ETP.ORG_PAD_IN_CODIGO = PAD_IN_CODIGO
                                                                                       AND ETP.ORG_IN_CODIGO     = IN_CODIGO
                                                                                       AND ETP.ORG_TAU_ST_CODIGO = TAU_ST_CODIGO
                                                                                       AND ETP.EST_IN_CODIGO     = BLO_PAI_EST
                                                                                       AND ETP.EST_CH_TIPOESTRUTURA = 'T')
                                                                             WHERE EMP.ORG_TAB_IN_CODIGO = TAB_IN_CODIGO
                                                                               AND EMP.ORG_PAD_IN_CODIGO = PAD_IN_CODIGO
                                                                               AND EMP.ORG_IN_CODIGO = IN_CODIGO
                                                                               AND EMP.ORG_TAU_ST_CODIGO = TAU_ST_CODIGO
                                                                               AND EMP.EST_IN_CODIGO = ETP_PAI_EST
                                                                               AND EMP.EST_CH_TIPOESTRUTURA = 'E'
                                                                               AND EMP.EST_IN_CODIGO = :vari ", new { vari = estCodigo });
            
        }
    }
}
