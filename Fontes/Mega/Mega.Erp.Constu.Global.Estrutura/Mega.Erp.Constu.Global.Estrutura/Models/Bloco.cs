﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.Constu.Global.Estrutura.Models
{
    public class Bloco
    {
        public string nome { set; get; }
        public string codigo { set; get; }
        public decimal est_in_codigo { set; get; }

        public Empreendimento estrutura { get; set; }
    }
}
