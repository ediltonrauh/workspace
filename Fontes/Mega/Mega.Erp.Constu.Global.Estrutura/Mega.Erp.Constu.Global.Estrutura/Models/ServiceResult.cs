﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.Constu.Global.Estrutura.Models
{
    public class ServiceResult
    {
        public object Result { get; set; }
        public Exception Error { get; set; }
    }
}
