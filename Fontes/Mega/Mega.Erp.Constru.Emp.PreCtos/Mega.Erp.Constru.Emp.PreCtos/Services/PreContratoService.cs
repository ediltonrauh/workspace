﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.Constru.Emp.PreCtos.Repository;
using Mega.Erp.Constru.Emp.PreCtos.Models;
using Mega.Erp.Constru.Emp.PreCtos.Controllers;
using Mega.Erp.Constru.Emp.Ctos.Services;
using Mega.Erp.Constru.Emp.Ctos.Repository;
using Mega.Erp.Constru.Emp.Ctos.Models;
using Mega.Erp.Constu.Global.Estrutura.Models;
using Mega.Erp.Constu.Global.Estrutura.Repository;
using Mega.Erp.Constu.Global.Estrutura.Services;

namespace Mega.Erp.Constru.Emp.PreCtos.Services
{
    public class PreContratoService
    {
        public IPreContratoRepository _repository;

        public PreContratoService(IPreContratoRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Contrato> GetPreContratos(PreContratoRepository connect)
        {

            // tentar converter Contratos => PreContrato
            var contratoRepository = new ContratoRepository(connect._connect);
            var contratoServico = new ContratoService(contratoRepository);
            IEnumerable<Contrato> contratos = contratoServico.Get("A");
            IEnumerable<Contrato> contratos2 = contratos.Where(c => c.cto_ch_origem.ToString().Equals("P"));

            return contratos2;
        }

        public IEnumerable<PreContrato> GetBlocos(PreContratoRepository connect, IEnumerable<PreContrato> precto)
        {
            var itemRepository = new ItemRepository(connect._connect);
            var itemtoServico = new ItemService(itemRepository);

            var apropItemRepository = new ApropriacaoItemRepository(connect._connect);
            var apropItemServico = new ApropriacaoItemService(apropItemRepository);

            var blocoRepository = new BlocoRepository(connect._connect);
            var blocoServico = new BlocoService(blocoRepository);
            
            for(int i = 0; i < precto.Count(); i++)
            {
                precto.ElementAt(i).blocos = blocoServico._repository.ListaBloco(Convert.ToDecimal(precto.ElementAt(i).est_in_codigo));
                precto.ElementAt(i).itens = itemtoServico._repository.ListaItens(precto.ElementAt(i));
                for(int j = 0; j< precto.ElementAt(i).itens.Count(); j++)
                {
                    precto.ElementAt(i).itens.ElementAt(j).apropriacao = apropItemServico._repository.ListaApropriacao(precto.ElementAt(i).itens.ElementAt(j));
                }
            }
            
            return precto ;

        }

        public void SalvaContratos(PreContratoRepository connect, IEnumerable<PreContrato> precto)
        {
            

           

        }
    }
}
