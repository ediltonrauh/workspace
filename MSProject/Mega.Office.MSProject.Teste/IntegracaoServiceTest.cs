﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mega.Office.MSProject.Models;
using Mega.Office.MSProject.Models.TypeOption;
using Mega.Office.MSProject.Services;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Mega.Office.MSProject.Teste
{
    [TestClass]
    public class IntegracaoServiceTest
    {
        [TestMethod]
        public void TestClassesToJson()
        {
            Integracao integracao = new Integracao();
            integracao.Arquivo = "TESTE.mpp";

            integracao.projeto = new Projeto();
            integracao.projeto.Autor = "Edilton Alex Rauh";
            integracao.projeto.DiasPorMes = 30;
            integracao.projeto.EnviaPeriodos = false;
            integracao.projeto.HorarioInicialPadrao = new TimeSpan(8, 0, 0); 
            integracao.projeto.HorarioFinalPadrao = new TimeSpan(16, 0, 0);
            integracao.projeto.HorasPorDia = 8;
            integracao.projeto.HorasPorSemana = 40;
            integracao.projeto.InicioDoProjeto = DateTime.Now;
            integracao.projeto.Nome = "Meu projeto";
            integracao.projeto.Organizacao = "Empreendimento teste";
            integracao.projeto.ProjectManager = "project my maneager";
            integracao.projeto.Titulo = "Titulo novo";

            Recurso _recurso = new Recurso();
            _recurso.Nome = "recurso 1";
            _recurso.ProjectID = 1;
            _recurso.Tipo = MsPjRecursoTypes.pjResourceTypeCost;
            _recurso.Custo = 1;
            List<Recurso> _recursos = new List<Recurso>();
            _recursos.Add(_recurso);
            integracao.projeto.Recursos = _recursos;

            Tarefa _tarefa = new Tarefa();
            _tarefa.DataInicial = Convert.ToDateTime("01/01/2017");
            _tarefa.Duracao = 20;
            _tarefa.Marco = 15;
            _tarefa.Nivel = 1;
            _tarefa.NiveldeIntegracao = MsNivelIntegracao.NivelFase;
            _tarefa.Nome = "Minha Tarefa";
            _tarefa.PercentualCompleto = 0;
            _tarefa.PercentualRealizado = 0;
            _tarefa.Posicao = 1;
            _tarefa.Sintetica = false;
            _tarefa.Tipo = MsPjTaskFixedType.pjFixedDuration;
            _tarefa.UltimoNivelFase = true;
            _tarefa.VerificaDesempenho = false;

            List<Tarefa> _Tarefas = new List<Tarefa>();
            _Tarefas.Add(_tarefa);
            integracao.projeto.Tarefas = _Tarefas;

            Atribuicao _atribuicao = new Atribuicao();
            _atribuicao.DataInicio = Convert.ToDateTime("01/01/2017");
            _atribuicao.DataFinal = Convert.ToDateTime("20/01/2017");
            _atribuicao.ProjectID = 1;
            //_atribuicao.RecursoID = 1;
            _atribuicao.SetaTrabalho = false;
            _atribuicao.SetaUnidade = false;
            _atribuicao.Trabalho = 5;
            _atribuicao.Unidade = 10;
            List<Atribuicao> _atribuicoes = new List<Atribuicao>();

            integracao.projeto.Atribuicoes = _atribuicoes;

            String _JsonData = JsonConvert.SerializeObject(integracao);
            System.IO.File.WriteAllText(@"D:\MyJSON_TEST.txt", _JsonData); 
        }

        [TestMethod]
        public void TestCriarOuAtualizar()
        {
            string _JsonData = System.IO.File.ReadAllText(@"D:\MyJSON_TEST.txt");
            IntegracaoService.CriarOuAtualizar(_JsonData);
        }
    }
}
