﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeComponent = Microsoft.Office.Interop.MSProject;
using Mega.Office.MSProject.Models;
using Mega.Office.MSProject.Services;

namespace Mega.Office.MSProject.Services
{
    static class CalendarioService
    {
        public static Boolean Localizar(OfficeComponent.Application msProjectApplication, string nomeCalendario)
        {
            foreach (OfficeComponent.Calendar calendar in msProjectApplication.GlobalBaseCalendars)
            {
                if (calendar.Name == nomeCalendario)
                {
                    return true;
                }
            }
            return false;
        }

        public static void Carregar(OfficeComponent.Application msProjectApplication, Models.Integracao integracao)
        {
            Object _Null = System.Reflection.Missing.Value;
            if (!Util.IsNullOrEmpty(integracao.Calendarios))
            {
                foreach (Calendario _Calendario in integracao.Calendarios)
                {
                    if (!Localizar(msProjectApplication, _Calendario.Nome))
                    {
                        if (msProjectApplication.BaseCalendarCreate(_Calendario.Nome, _Null))
                        {
                            msProjectApplication.BaseCalendarEditDays(_Calendario.Nome, _Null, _Null, OfficeComponent.PjWeekday.pjMonday, true, _Calendario.InicioTurno, _Calendario.FinalTurno, _Null, _Null, _Null, _Null, false, _Null, _Null, _Null, _Null);
                            msProjectApplication.BaseCalendarEditDays(_Calendario.Nome, _Null, _Null, OfficeComponent.PjWeekday.pjTuesday, true, _Calendario.InicioTurno, _Calendario.FinalTurno, _Null, _Null, _Null, _Null, false, _Null, _Null, _Null, _Null);
                            msProjectApplication.BaseCalendarEditDays(_Calendario.Nome, _Null, _Null, OfficeComponent.PjWeekday.pjWednesday, true, _Calendario.InicioTurno, _Calendario.FinalTurno, _Null, _Null, _Null, _Null, false, _Null, _Null, _Null, _Null);
                            msProjectApplication.BaseCalendarEditDays(_Calendario.Nome, _Null, _Null, OfficeComponent.PjWeekday.pjThursday, true, _Calendario.InicioTurno, _Calendario.FinalTurno, _Null, _Null, _Null, _Null, false, _Null, _Null, _Null, _Null);
                            msProjectApplication.BaseCalendarEditDays(_Calendario.Nome, _Null, _Null, OfficeComponent.PjWeekday.pjThursday, true, _Calendario.InicioTurno, _Calendario.FinalTurno, _Null, _Null, _Null, _Null, false, _Null, _Null, _Null, _Null);
                            msProjectApplication.BaseCalendarEditDays(_Calendario.Nome, _Null, _Null, OfficeComponent.PjWeekday.pjFriday, true, _Calendario.InicioTurno, _Calendario.FinalTurno, _Null, _Null, _Null, _Null, false, _Null, _Null, _Null, _Null);
                            msProjectApplication.BaseCalendarEditDays(_Calendario.Nome, _Null, _Null, OfficeComponent.PjWeekday.pjMonday, true, _Calendario.InicioTurno, _Calendario.FinalTurno, _Null, _Null, _Null, _Null, false, _Null, _Null, _Null, _Null);

                            if ((_Calendario.Tipo == 'C') || (_Calendario.Tipo == 'S'))
                            {
                                msProjectApplication.BaseCalendarEditDays(_Calendario.Nome, _Null, _Null, OfficeComponent.PjWeekday.pjSaturday, true, _Calendario.InicioTurno, _Calendario.FinalTurno, _Null, _Null, _Null, _Null, false, _Null, _Null, _Null, _Null);
                            }
                            else
                            {
                                msProjectApplication.BaseCalendarEditDays(_Calendario.Nome, _Null, _Null, OfficeComponent.PjWeekday.pjSaturday, false, _Null, _Null, _Null, _Null, _Null, _Null, false, _Null, _Null, _Null, _Null);
                            }

                            if (_Calendario.Tipo == 'C')
                            {
                                msProjectApplication.BaseCalendarEditDays(_Calendario.Nome, _Null, _Null, OfficeComponent.PjWeekday.pjSunday, true, _Calendario.InicioTurno, _Calendario.FinalTurno, _Null, _Null, _Null, _Null, false, _Null, _Null, _Null, _Null);
                            }
                            else
                            {
                                msProjectApplication.BaseCalendarEditDays(_Calendario.Nome, _Null, _Null, OfficeComponent.PjWeekday.pjSunday, false, _Null, _Null, _Null, _Null, _Null, _Null, false, _Null, _Null, _Null, _Null);
                            }
                        }
                    }

                    InserirFeriados(msProjectApplication, _Calendario);
                }
            }
        }

        private static void InserirFeriados(OfficeComponent.Application msProjectApplication, Calendario calendario)
        {
            Object _Null = System.Reflection.Missing.Value;
            if (!Util.IsNullOrEmpty(calendario.Feriados))
            {
                foreach (DateTime _DataFeriado in calendario.Feriados)
                {
                    msProjectApplication.BaseCalendarEditDays(calendario.Nome, _DataFeriado, _DataFeriado, 0, false, _Null, _Null, _Null, _Null, _Null, _Null, false, _Null, _Null, _Null, _Null);
                }
            }
        }
    }
}
