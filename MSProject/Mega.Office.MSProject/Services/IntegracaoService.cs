﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeComponent = Microsoft.Office.Interop.MSProject; 
using Mega.Office.MSProject.Models;
using Newtonsoft.Json;

namespace Mega.Office.MSProject.Services
{

    public static class IntegracaoService
    {

        public static String CriarOuAtualizar(string JsonData)
        {
            Models.Integracao _IntegracaoModel = JsonConvert.DeserializeObject<Integracao>(JsonData);
            OfficeComponent.Application _MsProjectApplication = new OfficeComponent.Application();
            OfficeComponent.Project _MsProject = ProjetoService.Carregar(_MsProjectApplication, _IntegracaoModel);

            CalendarioService.Carregar(_MsProjectApplication, _IntegracaoModel);

            TarefaService.Carregar(_MsProject, _IntegracaoModel);

            RecursoService.Carregar(_MsProject, _IntegracaoModel);

            AtribuicaoService.Carregar(_MsProject, _IntegracaoModel);

            _MsProject.SaveAs(_IntegracaoModel.Arquivo);
            Object _Null = System.Reflection.Missing.Value;
            _MsProjectApplication.OptionsCalculation(false, _Null, _Null, _Null, _Null, _Null, _Null, _Null, _Null, _Null, _Null, _Null, _Null, _Null, _Null, _Null);
            _MsProjectApplication.ScreenUpdating = false;

            return (_IntegracaoModel.Arquivo);
        }

        public static String LerArquivo(string value)
        {
            return ("");
        }

    }
}