﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeComponent = Microsoft.Office.Interop.MSProject;
using Mega.Office.MSProject.Models;

namespace Mega.Office.MSProject.Services
{
    static class RecursoService
    {
        public static OfficeComponent.Resource Localizar(OfficeComponent.Project msProject, int recursoID)
        {
            OfficeComponent.Resource _Retorno = null;
            foreach (OfficeComponent.Resource _Resource in msProject.Resources)
            {
                if (_Resource.UniqueID == recursoID)
                {
                    _Retorno = _Resource;
                    break;
                }
            }
            return _Retorno;
        }

        public static OfficeComponent.Resource Manter(OfficeComponent.Project msProject, Recurso recursoModel)
        {
            OfficeComponent.Resource _MsResource = Localizar(msProject, recursoModel.ProjectID);
            if (_MsResource == null)
            {
                _MsResource = msProject.Resources.Add(recursoModel.Nome, (msProject.Resources.Count + 1));
            }
            return _MsResource;
        }

        public static OfficeComponent.Resource Criar(OfficeComponent.Project msProject, Recurso recursoModel)
        {
            OfficeComponent.Resource _MsRecource = Manter(msProject, recursoModel);
            _MsRecource.Name = recursoModel.Nome;
            _MsRecource.Type = OfficeComponent.PjResourceTypes.pjResourceTypeMaterial;
            _MsRecource.StandardRate = recursoModel.Custo;

            if (_MsRecource.UniqueID != recursoModel.ProjectID)
            {
                recursoModel.ProjectID = _MsRecource.UniqueID;
            }

            return _MsRecource;
        }

        public static void Carregar(OfficeComponent.Project msProject, Models.Integracao integracao)
        {
            if (!Util.IsNullOrEmpty(integracao.projeto.Recursos))
            {
                foreach (Models.Recurso _RecursoModel in integracao.projeto.Recursos)
                {
                    Criar(msProject, _RecursoModel);
                }
            }
        }

    }
}
