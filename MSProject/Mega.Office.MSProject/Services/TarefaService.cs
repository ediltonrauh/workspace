﻿using System;
using System.Linq;
using OfficeComponent = Microsoft.Office.Interop.MSProject;
using Mega.Office.MSProject.Models;
using System.Collections.Generic;
using Mega.Office.MSProject.Models.TypeOption;

namespace Mega.Office.MSProject.Services
{
    static class TarefaService
    {
        public static OfficeComponent.Task Localizar(OfficeComponent.Project msProject, int tarefaID)
        {
            OfficeComponent.Task _Retorno = null;
            foreach (OfficeComponent.Task _MsTask in msProject.Tasks)
            {
                if (_MsTask.UniqueID == tarefaID)
                {
                    _Retorno = _MsTask;
                    break;
                }
            }
            return _Retorno;
        }

        public static OfficeComponent.Task Manter(OfficeComponent.Project msProject, Tarefa tarefaModel)
        {
            OfficeComponent.Task _MsTask = Localizar(msProject, tarefaModel.ProjectID);
            if (_MsTask == null)
            {
                _MsTask = msProject.Tasks.Add(tarefaModel.Nome, tarefaModel.Posicao);
                _MsTask.Estimated = false;
            }
            return _MsTask;
        }

        public static OfficeComponent.Task Criar(OfficeComponent.Project msProject, Tarefa tarefaModel, Boolean enviaPeriodos)
        {

            OfficeComponent.Task _MsTask = Manter(msProject, tarefaModel);

            _MsTask.Manual = (Convert.ToInt32(msProject.VersionName) < 150);
            _MsTask.Name = tarefaModel.Nome;
            _MsTask.OutlineLevel = tarefaModel.Nivel;
            _MsTask.Milestone = tarefaModel.Marco;
            _MsTask.Calendar = tarefaModel.Calendario;
            //_MsTask.IgnoreResourceCalendar = true;

            if (tarefaModel.Marco == 1)
            {
                _MsTask.Start = tarefaModel.DataInicial;
                _MsTask.Duration = tarefaModel.Duracao;
            }

            if ((tarefaModel.Sintetica) ||
                ((tarefaModel.NiveldeIntegracao == MsNivelIntegracao.NivelFase) && tarefaModel.UltimoNivelFase))
            {

                _MsTask.Start = tarefaModel.DataInicial;
                _MsTask.Type = (OfficeComponent.PjTaskFixedType)tarefaModel.Tipo;

                if (tarefaModel.RestricaoDeData)
                {
                    _MsTask.ConstraintType = OfficeComponent.PjConstraint.pjASAP;
                }
                else
                {
                    _MsTask.ConstraintType = OfficeComponent.PjConstraint.pjSNET;
                    _MsTask.ConstraintDate = tarefaModel.DataInicial;
                }

                if (new[] { MsNivelIntegracao.NivelServico, MsNivelIntegracao.NivelFase }.Contains(tarefaModel.NiveldeIntegracao))
                {
                    _MsTask.Duration = tarefaModel.Duracao;
                }

                _MsTask.EffortDriven = (tarefaModel.VerificaDesempenho);
                _MsTask.PercentComplete = tarefaModel.PercentualRealizado;

                if ((tarefaModel.NiveldeIntegracao == MsNivelIntegracao.NivelInsumo) && (enviaPeriodos))
                {
                    Boolean _existeTrabalho = false;
                    if (_MsTask.Resources.Count == 0)
                    {
                        _existeTrabalho = true;
                    }
                    else
                    {
                        foreach (OfficeComponent.Resource _recursoTarefa in _MsTask.Resources)
                        {
                            if (_recursoTarefa.Type == OfficeComponent.PjResourceTypes.pjResourceTypeWork)
                            {
                                _existeTrabalho = true;
                                break;
                            }
                        }
                    }
                    if (_existeTrabalho)
                    {
                        _MsTask.PercentWorkComplete = tarefaModel.PercentualCompleto;
                    }
                }
            }
            if (_MsTask.UniqueID != tarefaModel.ProjectID)
            {
                tarefaModel.ProjectID = _MsTask.UniqueID;
            }

            return _MsTask;
        }

        public static void Carregar(OfficeComponent.Project msProject, Models.Integracao integracao)
        {
            if (!Util.IsNullOrEmpty(integracao.projeto.Tarefas))
            {
                foreach (Models.Tarefa _TarefaModel in integracao.projeto.Tarefas)
                {
                    Criar(msProject, _TarefaModel, integracao.projeto.EnviaPeriodos);
                }
            }
        }

    }
}
