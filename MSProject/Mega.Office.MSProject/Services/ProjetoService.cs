﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeComponent = Microsoft.Office.Interop.MSProject;
using Mega.Office.MSProject.Models;


namespace Mega.Office.MSProject.Services
{
    static class ProjetoService
    {
        private const string RepositoryFile = "D:\\";

        public static OfficeComponent.Project Carregar(OfficeComponent.Application msProjectApplication, Integracao integracaoModel)
        {
            Object _Null = System.Reflection.Missing.Value;
            OfficeComponent.Project _MsProject = null;
            integracaoModel.Arquivo = (RepositoryFile + integracaoModel.Arquivo);

            if (System.IO.File.Exists(integracaoModel.Arquivo))
            {
                msProjectApplication.FileOpenEx(integracaoModel.Arquivo);
                _MsProject = msProjectApplication.ActiveProject;
            }
            else
            {
                _MsProject = msProjectApplication.Projects.Add(_Null, _Null, _Null);
            }

            Atualizar(_MsProject, integracaoModel.projeto);
            return _MsProject;
        }

        public static void Atualizar(OfficeComponent.Project msProject, Projeto projetoModel)
        {
            msProject.Name = projetoModel.Nome;
            msProject.Title = projetoModel.Titulo;
            msProject.Author = projetoModel.Autor;
            msProject.Company = projetoModel.Organizacao;
            msProject.Manager = projetoModel.ProjectManager;
            msProject.DefaultStartTime = projetoModel.HorarioInicialPadrao;
            msProject.DefaultFinishTime = projetoModel.HorarioFinalPadrao;
            msProject.HoursPerDay = projetoModel.HorasPorDia;
            msProject.HoursPerWeek = projetoModel.HorasPorSemana;
            msProject.DaysPerMonth = projetoModel.DiasPorMes;
            msProject.ProjectStart = projetoModel.InicioDoProjeto;
        }

    }
}
