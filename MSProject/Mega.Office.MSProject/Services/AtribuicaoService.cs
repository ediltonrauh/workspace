﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeComponent = Microsoft.Office.Interop.MSProject;
using Mega.Office.MSProject.Models;
using Mega.Office.MSProject.Services;

namespace Mega.Office.MSProject.Services
{
    static class AtribuicaoService
    {
        public static OfficeComponent.Assignment Localizar(OfficeComponent.Task msTask, int RecursoId, int AtribuicaoId)
        {
            OfficeComponent.Assignment _Retorno = null;
            foreach (OfficeComponent.Assignment _MsAssignment in msTask.Assignments)
            {
                if ((_MsAssignment.UniqueID == AtribuicaoId) &&
                        (_MsAssignment.ResourceID == RecursoId))
                {
                    _Retorno = _MsAssignment;
                    break;
                }
            }
            return _Retorno;
        }

        public static OfficeComponent.Assignment Manter(OfficeComponent.Project msProject, Projeto projeto ,Atribuicao atribuicaoModel)
        {
            int projIDTarefa = projeto.RetornaProjectIdTarefa(atribuicaoModel.TarefaChaveExterna);
            int projIDRecurso = projeto.RetornaProjectIdRecurso(atribuicaoModel.RecursoChaveExterna);

            OfficeComponent.Task _MsTask = TarefaService.Localizar(msProject, projIDTarefa);
            OfficeComponent.Assignment _MsAssignment = Localizar(_MsTask, projIDRecurso, atribuicaoModel.ProjectID);

            if (_MsAssignment == null)
            {
                _MsAssignment = _MsTask.Assignments.Add(projIDTarefa, projIDRecurso);
            }
            return _MsAssignment;
        }


        public static OfficeComponent.Assignment Criar(OfficeComponent.Project msProject, Projeto projeto ,Atribuicao atribuicaoModel)
        {
            OfficeComponent.Assignment _MsAssigment = Manter(msProject, projeto, atribuicaoModel);

            if (atribuicaoModel.SetaUnidade)
            {
                _MsAssigment.Units = atribuicaoModel.Unidade;
            }

            if (atribuicaoModel.SetaTrabalho)
            {
                _MsAssigment.Work = atribuicaoModel.Trabalho;
            }

            _MsAssigment.Start = atribuicaoModel.DataInicio;
            _MsAssigment.Finish = atribuicaoModel.DataFinal;

            if (_MsAssigment.UniqueID != atribuicaoModel.ProjectID)
            {
                atribuicaoModel.ProjectID = _MsAssigment.UniqueID;
            }

            return _MsAssigment;
        }

        public static void Carregar(OfficeComponent.Project msProject, Models.Integracao integracao)
        {
            if (!Util.IsNullOrEmpty(integracao.projeto.Atribuicoes))
            {
                foreach (Models.Atribuicao _AtribuicaoModel in integracao.projeto.Atribuicoes)
                {
                    Criar(msProject, integracao.projeto, _AtribuicaoModel);
                }
            }
        }

    }
}
