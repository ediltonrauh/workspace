﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeComponent = Microsoft.Office.Interop.MSProject;

namespace Mega.Office.MSProject.Models.TypeOption
{
    public enum MsPjRecursoTypes
    {
        pjResourceTypeCost = OfficeComponent.PjResourceTypes.pjResourceTypeCost,
        pjResourceTypeMaterial = OfficeComponent.PjResourceTypes.pjResourceTypeMaterial,
        pjResourceTypeWork = OfficeComponent.PjResourceTypes.pjResourceTypeWork
    };

    public enum MsPjTaskFixedType
    {
        pjFixedDuration = OfficeComponent.PjTaskFixedType.pjFixedDuration,
        pjFixedUnits = OfficeComponent.PjTaskFixedType.pjFixedUnits,
        pjFixedWork = OfficeComponent.PjTaskFixedType.pjFixedWork,
    }
     

    public enum MsNivelIntegracao
    {
        NivelInsumo = 0,
        NivelServico = 1,
        NivelFase = 2
    };

    public enum MsPjTaskTimescaledData
    {
        pjTaskTimescaledActualWork = OfficeComponent.PjTaskTimescaledData.pjTaskTimescaledActualWork,
        pjTaskTimescaledWork = OfficeComponent.PjTaskTimescaledData.pjTaskTimescaledWork
    }

    public enum MsPjTimescaleUnit
    {
        pjTimescaleDays = OfficeComponent.PjTimescaleUnit.pjTimescaleDays,
        pjTimescaleWeeks = OfficeComponent.PjTimescaleUnit.pjTimescaleWeeks,
        pjTimescaleMonths = OfficeComponent.PjTimescaleUnit.pjTimescaleMonths

    }

}
