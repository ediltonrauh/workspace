﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OfficeComponent = Microsoft.Office.Interop.MSProject;
using Mega.Office.MSProject.Models.TypeOption;

namespace Mega.Office.MSProject.Models
{

    public class Recurso 
    {
        public string ChaveExterna { get; set; }
        public int ProjectID { get; set; }
        public string Nome { get; set; }
        public MsPjRecursoTypes Tipo { get; set; }
        public Double Custo { get; set; }
    }
}