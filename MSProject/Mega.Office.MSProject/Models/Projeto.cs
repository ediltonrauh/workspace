﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Office.MSProject.Models
{
    public class Projeto
    {
        public string Nome { get; set; }
        public string Titulo { get; set; }
        public string Autor { get; set; }
        public string Organizacao { get; set; }
        public string ProjectManager { get; set; }
        public TimeSpan HorarioInicialPadrao { get; set; }
        public TimeSpan HorarioFinalPadrao { get; set; }
        public double HorasPorDia { get; set; }
        public double HorasPorSemana { get; set; }
        public double DiasPorMes { get; set; }
        public DateTime InicioDoProjeto { get; set; }
        public IEnumerable<Tarefa> Tarefas { get; set; }
        public IEnumerable<Recurso> Recursos { get; set; }
        public IEnumerable<Atribuicao> Atribuicoes { get; set; }
        public Boolean EnviaPeriodos { get; set; }

        public Tarefa RetornaTarefa(string chaveexterna)
        {
            Tarefa retorno = Tarefas.Single(x => x.ChaveExterna == chaveexterna);
            return retorno;
        }
        
        public int RetornaProjectIdTarefa(string chaveexterna)
        {
            Tarefa tarefa = RetornaTarefa(chaveexterna);
            if (tarefa == null)
                return -1;
            else
                return tarefa.ProjectID;
        }

        public int RetornaProjectIdRecurso(string chaveexterna)
        {
            Recurso retorno = Recursos.Single(x => x.ChaveExterna == chaveexterna);
            if (retorno == null)
                return -1;
            else
                return retorno.ProjectID;
        }
    }

}
