﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Office.MSProject.Models
{
    public class Integracao
    {
        public string Arquivo { get; set; }
        public Projeto projeto { get; set; }
        public IEnumerable<Calendario> Calendarios { get; set; }        
    }
}
