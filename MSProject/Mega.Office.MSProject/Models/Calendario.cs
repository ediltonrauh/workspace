﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Office.MSProject.Models
{
    public class Calendario
    {
        public string Nome { get; set; }
        public char Tipo { get; set; }
        public string InicioTurno { get; set; }
        public string FinalTurno { get; set; }
        public IEnumerable<DateTime> Feriados { get; set; }
    }
}
