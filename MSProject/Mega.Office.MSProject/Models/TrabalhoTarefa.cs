﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mega.Office.MSProject.Models.TypeOption;

namespace Mega.Office.MSProject.Models
{
    public class TrabalhoTarefa
    {
        public MsPjTimescaleUnit EscalaDeTempo { get; set; }
        public MsPjTaskTimescaledData EscalaDeTempoTrabalho { get; set; } //Antigo Trab_Real
        public float Trabalho { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFinal { get; set; }
    }
}
