﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mega.Office.MSProject.Models
{
    public class Atribuicao
    {
        public int ProjectID { get; set; }
        public string TarefaChaveExterna { get; set; }
        public string RecursoChaveExterna { get; set; }
        public bool SetaUnidade { get; set; }
        public decimal Unidade { get; set; }        
        public bool SetaTrabalho { get; set; }
        public decimal Trabalho { get; set; }        
        public DateTime DataInicio { get; set; }
        public DateTime DataFinal { get; set; }
    }
}
