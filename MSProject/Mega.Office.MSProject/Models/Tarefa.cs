﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Office.MSProject.Models.TypeOption;

namespace Mega.Office.MSProject.Models
{
    public class Tarefa
    {
        public string ChaveExterna { get; set; }
        public string ChaveExternaTarefaPai { get; set; }
        public int ProjectID { get; set; }
        public string Nome { get; set; }
        public short Nivel { get; set; }
        public int Posicao { get; set; }
        public int Marco { get; set; }
        public string Calendario { get; set; }
        public DateTime DataInicial { get; set; }
        public MsPjTaskFixedType Tipo { get; set; }
        public bool Sintetica { get; set; }
        public decimal Duracao { get; set; }
        public bool RestricaoDeData { get; set; }
        public MsNivelIntegracao NiveldeIntegracao { get; set; } 
        public bool VerificaDesempenho { get; set; }
        public decimal PercentualRealizado { get; set; }
        public decimal PercentualCompleto { get; set; }
        public bool UltimoNivelFase { get; set; }
        public IEnumerable<TrabalhoTarefa> Trabalho { get; set; }
    }

    
}