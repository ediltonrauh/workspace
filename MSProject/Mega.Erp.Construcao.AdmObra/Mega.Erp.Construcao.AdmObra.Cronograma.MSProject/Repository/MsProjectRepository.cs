﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Office.MSProject.Models;
using Mega.Office.MSProject.Models.TypeOption;

namespace Mega.Erp.Construcao.AdmObra.Cronograma.MSProject.Repository
{
    public class MsProjectRepository
    { 
        public Integracao Get()
        {
            Integracao integracao = new Integracao();
            ExemploCalendario(integracao);
            integracao.projeto = new Projeto();
            InsereDadosProjeto(integracao.projeto);
            integracao.projeto.Tarefas = new List<Tarefa>();
            ExemploTarefas((List<Tarefa>)integracao.projeto.Tarefas);
            integracao.projeto.Recursos = new List<Recurso>();
            ExemploRecursos((List<Recurso>)integracao.projeto.Recursos);
            integracao.projeto.Atribuicoes = new List<Atribuicao>();
            ExemploAtribuicao((List<Atribuicao>)integracao.projeto.Atribuicoes);            
            return integracao;
        }

        public void InsereDadosProjeto(Projeto projeto)
        {
            projeto.Nome = "873-Planejamento_inicial_-_Nivel_insumo";
            projeto.Titulo = "873-Planejamento_inicial_-_Nivel_insumo";
            projeto.Autor = "mega";
            projeto.Organizacao = "RITMO EMPREENDIMENTOS LTDA";
            projeto.ProjectManager = "mega";
            projeto.HorarioInicialPadrao = new TimeSpan(8, 0, 0);
            projeto.HorarioFinalPadrao = new TimeSpan(16, 0, 0);
            projeto.HorasPorDia = 8;
            projeto.HorasPorSemana = 44;
            projeto.DiasPorMes = 26;
            projeto.InicioDoProjeto = new DateTime(2019, 1, 2);
            
        }

        public void InsereDadosCalendarios(Calendario calendario, string nome, Char tipoCalendario, string inicioTurno, string finalTurno)
        {
            calendario.Nome = nome;
            calendario.Tipo = tipoCalendario;
            calendario.InicioTurno = inicioTurno;
            calendario.FinalTurno = finalTurno;
        }

        public static Tarefa InsereTarefa(string ChaveExterna,
                                          int ProjectID,
                                          string Nome,
                                          short Nivel,
                                          int Posicao,
                                          int Marco,
                                          string Calendario,
                                          DateTime DataInicial,
                                          int tipo,
                                          bool Sintetica,
                                          decimal Duracao,
                                          bool RestricaoDeData,
                                          MsNivelIntegracao NiveldeIntegracao,
                                          bool VerificaDesempenho,
                                          decimal PercentualRealizado,
                                          decimal PercentualCompleto,
                                          bool UltimoNivelFase)
        {
            Tarefa tarefa = new Tarefa();
            tarefa.ChaveExterna = ChaveExterna;
            tarefa.ProjectID = ProjectID;
            tarefa.Nome = Nome;
            tarefa.Nivel = Nivel;
            tarefa.Marco = Marco;
            tarefa.Posicao = Posicao;
            tarefa.Calendario = Calendario;
            tarefa.DataInicial = DataInicial;
            //tarefa.Tipo = Tipo;
            tarefa.Sintetica = Sintetica;
            tarefa.Duracao = Duracao;
            tarefa.RestricaoDeData = RestricaoDeData;
            tarefa.NiveldeIntegracao = NiveldeIntegracao;
            tarefa.VerificaDesempenho = VerificaDesempenho;
            tarefa.PercentualRealizado = PercentualRealizado;
            tarefa.PercentualCompleto = PercentualCompleto;
            tarefa.UltimoNivelFase = UltimoNivelFase;
            return tarefa;
        }

        public static Recurso InsereRecurso(string ChaveExterna, int ProjectID, string Nome, int Tipo, double Custo)
        {
            Recurso recurso = new Recurso();
            recurso.ChaveExterna = ChaveExterna;
            recurso.ProjectID = ProjectID;
            recurso.Nome = Nome;
            //recurso.Tipo = Tipo;
            recurso.Custo = Custo;
            return recurso;
        }

        public static Atribuicao InsereAtribuicao(int ProjectID,
                                                  string TarefaChaveExterna,
                                                  string RecursoChaveExterna,
                                                  bool SetaUnidade,
                                                  decimal Unidade,
                                                  bool SetaTrabalho,
                                                  decimal Trabalho,
                                                  DateTime DataInicio,
                                                  DateTime DataFinal)
        {
            Atribuicao atribuicao = new Atribuicao();
            atribuicao.ProjectID = ProjectID;
            atribuicao.TarefaChaveExterna = TarefaChaveExterna;
            atribuicao.RecursoChaveExterna = RecursoChaveExterna;
            atribuicao.SetaUnidade = SetaUnidade;
            atribuicao.Unidade = Unidade;
            atribuicao.SetaTrabalho = SetaTrabalho;
            atribuicao.Trabalho = Trabalho;
            atribuicao.DataInicio = DataInicio;
            atribuicao.DataFinal = DataFinal;
            return atribuicao;
        }

        public void ExemploTarefas(List<Tarefa> tarefas)
        {
            tarefas.Add(InsereTarefa("F94550",
                                         0,
                                         "SERVIÇOS PRELIMINARES",
                                         1,
                                         1,
                                         0,
                                         "Calendário Padrão",
                                         new DateTime(2019, 01, 02, 08, 00, 00),
                                         3,
                                         true,
                                         67200,
                                         true,
                                         MsNivelIntegracao.NivelInsumo,
                                         true,
                                         0,
                                         0,
                                         false));

            tarefas.Add(InsereTarefa("S314683",
                                         0,
                                         "Instalações de Canteiro - Instalações Provisórias",
                                         2,
                                         2,
                                         0,
                                         "Calendário Padrão",
                                         new DateTime(2019, 01, 02, 08, 00, 00),
                                         3,
                                         false,
                                         1440,
                                         true,
                                         MsNivelIntegracao.NivelInsumo,
                                         true,
                                         0,
                                         0,
                                         false));

            tarefas.Add(InsereTarefa("S314684",
                                         0,
                                         "Carga mecanizada de entulho transporte até o 1°km",
                                         2,
                                         3,
                                         0,
                                         "Calendário Padrão",
                                         new DateTime(2019, 01, 02, 08, 00, 00),
                                         3,
                                         false,
                                         28800,
                                         true,
                                         MsNivelIntegracao.NivelInsumo,
                                         true,
                                         0,
                                         0,
                                         false));
            tarefas.Add(InsereTarefa("S314685",
                                         0,
                                         "Escavação manual em solo qualquer categoria",
                                         2,
                                         4,
                                         0,
                                         "Calendário Padrão",
                                         new DateTime(2019, 03, 27, 08, 00, 00),
                                         3,
                                         false,
                                         38400,
                                         true,
                                         MsNivelIntegracao.NivelInsumo,
                                         true,
                                         0,
                                         0,
                                         false));

            tarefas.Add(InsereTarefa("F94550",
                                         0,
                                         "PAVIMENTAÇÃO",
                                         1,
                                         5,
                                         0,
                                         "Calendário Padrão",
                                         new DateTime(2019, 07, 17, 08, 00, 00),
                                         3,
                                         true,
                                         86400,
                                         true,
                                         MsNivelIntegracao.NivelInsumo,
                                         true,
                                         0,
                                         0,
                                         false));

            tarefas.Add(InsereTarefa("S314690",
                                         0,
                                         "Base de Rachão",
                                         2,
                                         6,
                                         0,
                                         "Calendário Padrão",
                                         new DateTime(2019, 07, 17, 08, 00, 00),
                                         3,
                                         false,
                                         20640,
                                         true,
                                         MsNivelIntegracao.NivelInsumo,
                                         true,
                                         0,
                                         0,
                                         false));

            tarefas.Add(InsereTarefa("S314691",
                                         0,
                                         "Colchao drenante - Material de enchimento - areia - (incl. transp. até 3 km)",
                                         2,
                                         7,
                                         0,
                                         "Calendário Padrão",
                                         new DateTime(2019, 07, 17, 08, 00, 00),
                                         3,
                                         false,
                                         24000,
                                         true,
                                         MsNivelIntegracao.NivelInsumo,
                                         true,
                                         0,
                                         0,
                                         false));

            tarefas.Add(InsereTarefa("S314692",
                                         0,
                                         "Pavimentação - CBUQ (Faixa C/ DNER)",
                                         2,
                                         8,
                                         0,
                                         "Calendário Padrão",
                                         new DateTime(2019, 09, 25, 08, 00, 00),
                                         3,
                                         false,
                                         62400,
                                         true,
                                         MsNivelIntegracao.NivelInsumo,
                                         true,
                                         0,
                                         0,
                                         false));

        }

        public void ExemploRecursos(List<Recurso> recursos)
        {
            recursos.Add(InsereRecurso("12353", 0, "Transporte Asfalto", 1, 300));
            recursos.Add(InsereRecurso("12376", 0, "Material de enchimento areia", 1, 22));
            recursos.Add(InsereRecurso("12397", 0, "Servente", 1, 2.4));
            recursos.Add(InsereRecurso("12517", 0, "Servente descarga Mat. rede aérea", 1, 2.4));
            recursos.Add(InsereRecurso("12805", 0, "Aluguel de Oficina/Almoxarifado", 1, 2000));
            recursos.Add(InsereRecurso("21523", 0, "Carregadeira Cat 966H (262 Hp) -  4,2 m3", 1, 200));
            recursos.Add(InsereRecurso("21844", 0, "Motoniveladora CAT 120H 140 hp", 1, 2000));
            recursos.Add(InsereRecurso("21874", 0, "Rolo Compact.Asfalto Tanden Dynapac CC524 HF 154 kn 110 Hp", 1, 2000));
        }

        public void ExemploAtribuicao(List<Atribuicao> atribuicoes)
        {
            atribuicoes.Add(InsereAtribuicao(0, "S314683", "12517", true, 0, false, 0, new DateTime(2019, 02, 01), new DateTime(2019, 01, 04)));
            atribuicoes.Add(InsereAtribuicao(0, "S314683", "12517", true, 0, false, 0, new DateTime(2019, 01, 02), new DateTime(2019, 01, 04)));
            atribuicoes.Add(InsereAtribuicao(0, "S314683", "12805", true, 0, false, 0, new DateTime(2019, 01, 02), new DateTime(2019, 01, 04)));
            atribuicoes.Add(InsereAtribuicao(0, "S314684", "12397", true, 0, false, 0, new DateTime(2019, 01, 02), new DateTime(2019, 03, 26)));
            atribuicoes.Add(InsereAtribuicao(0, "S314684", "21523", true, 0, false, 0, new DateTime(2019, 01, 02), new DateTime(2019, 03, 26)));
            atribuicoes.Add(InsereAtribuicao(0, "S314685", "12397", true, 0, false, 0, new DateTime(2019, 03, 27), new DateTime(2019, 07, 16)));
            atribuicoes.Add(InsereAtribuicao(0, "S314690", "12397", true, 0, false, 0, new DateTime(2019, 07, 17), new DateTime(2019, 09, 13)));
            atribuicoes.Add(InsereAtribuicao(0, "S314690", "21844", true, 0, false, 0, new DateTime(2019, 07, 17), new DateTime(2019, 09, 13)));
            atribuicoes.Add(InsereAtribuicao(0, "S314691", "12376", true, 0, false, 0, new DateTime(2019, 07, 17), new DateTime(2019, 09, 24)));
            atribuicoes.Add(InsereAtribuicao(0, "S314692", "12353", true, 0, false, 0, new DateTime(2019, 09, 25), new DateTime(2020, 03, 24)));
            atribuicoes.Add(InsereAtribuicao(0, "S314692", "21874", true, 0, false, 0, new DateTime(2019, 09, 25), new DateTime(2020, 03, 24)));

        }

        public void ExemploCalendario(Integracao integracao)
        {
            Calendario CalendarioTeste = new Calendario();
            InsereDadosCalendarios(CalendarioTeste, "Calendário Padrão", 'S', "08:00", "16:00");
            ExemploFeriados(CalendarioTeste);
            integracao.Calendarios = new List<Calendario>();
            ((List<Calendario>)integracao.Calendarios).Add(CalendarioTeste);            
        }

        public void ExemploFeriados(Calendario CalendarioTeste)
        {
            CalendarioTeste.Feriados = new List<DateTime>() { new DateTime(2018, 01, 01), new DateTime(2018, 03, 07) };
        }
    }
}
