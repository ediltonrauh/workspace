﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.Construcao.AdmObra.Cronograma.MSProject.Models
{
    public class CronoProject
    {
        public int ORC_IN_CODIGO { get; set; }
        public int ORC_IN_SEQUENCIA { get; set; }
        public int CRO_IN_CODIGO { get; set; }
        public char PID_CH_TIPO { get; set; }
        public int PID_IN_CODPROJECT { get; set; }
        public int SVC_IN_CODIGO { get; set; }
        public int PAI_SVC_IN_CODIGO { get; set; } 
        public int FASE_IN_CODIGO { get; set; }
        public int PRO_TAB_IN_CODIGO { get; set; }
        public int PRO_PAD_IN_CODIGO { get; set; } 
        public int PRO_IN_CODIGO { get; set; }
    }
}
