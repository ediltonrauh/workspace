﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mega.Erp.Construcao.AdmObra.Cronograma.MSProject.Repository;

namespace Mega.Erp.Construcao.AdmObra.Cronograma.MSProject.Controllers
{
    public class ServiceResult
    {
        public object Result { get; set; }
        public Exception Error { get; set; }
    }

    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        public IActionResult Get()
        {
            MsProjectRepository Repository = new MsProjectRepository();
            //var result = new ServiceResult();
            //result.Result =  Repository.Get();
            return Ok(Repository.Get());
        }

    }
}
